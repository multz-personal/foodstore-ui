import * as React from 'react'
import {useForm} from "react-hook-form";
import {useDispatch} from "react-redux";
import {Link, useHistory} from "react-router-dom";
import {login} from "../../api/auth";
import {userLogin} from "../../features/Auth/actions";
import {Button, Card, FormControl, InputPassword, InputText, LayoutOne} from "upkit";
import StoreLogo from "../../components/StoreLogo";
import {rules} from "./validation";

const statusList = {
  idle: 'idle',
  process: 'process',
  success: 'success',
  error: 'error',
}

export default function Login() {
  const {register, handleSubmit, errors, setError} = useForm()
  const [status, setStatus] = React.useState(statusList.idle)
  const dispatch = useDispatch()
  const history = useHistory()

  const onSubmit = async ({email, password}) => {
    setStatus(statusList.process)
    let {data} = await login(email, password)
    if (data.error) {
      setError('password', {type:'invalidCredential', message: data.message})
      setStatus(statusList.error)
    } else {
      let {user, token} = data
      dispatch(userLogin(user, token))
      history.push("/")
    }

    setStatus(statusList.success)
  }

  return (
    <LayoutOne size="small">
      <br/>
      <Card color="white">
        <div className="text-center mb-5">
          <StoreLogo />
        </div>

        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl errorMessage={errors.email?.message}>
            <InputText
              placeholder="email"
              fitContainer
              name="email"
              ref={register(rules.email)}
            />
          </FormControl>

          <FormControl errorMessage={errors.password?.message}>
            <InputPassword
              placeholder="password"
              name="password"
              fitContainer
              ref={register(rules.password)}
            />
          </FormControl>

          <Button fitContainer size="large" disabled={status === 'process'}>Login</Button>
        </form>
        <div className="text-center mt-2">
          Belum punya akun? <Link to="/register"><strong>Daftar sekarang.</strong></Link>
        </div>
      </Card>
    </LayoutOne>
  )
}
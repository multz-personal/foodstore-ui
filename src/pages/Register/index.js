import * as React from 'react'
import { FormControl, Card, InputPassword, InputText, LayoutOne, Button } from 'upkit'
import { useForm } from 'react-hook-form'
import { rules } from './validation'
import {registerUser} from '../../api/auth'
import {Link, useHistory} from "react-router-dom";
import StoreLogo from "../../components/StoreLogo";

const statusList = {
  idle: 'idle',
  process: 'process',
  success: 'success',
  error: 'error'
}

export default function Register() {
  let { register, handleSubmit, formState:{ errors }, setError } = useForm()
  let [status, setStatus] = React.useState(statusList.idle)
  let history = useHistory()

  const onSubmit = async formData => {
    let {password, password_confirmation} = formData
    if (password !== password_confirmation) {
      return setError('password_confirmation', {type: 'equality', message:'Konfirmasi password harus sama dengan password.'})
    }
    setStatus(statusList.process)
    let {data} = await registerUser(formData)

    if (data.error) {
      let fields = Object.keys(data.fields)

      fields.forEach(field => {
        setError(field, {type: 'server', message: data.fields[field]?.properties?.message})
      })

      setStatus(statusList.error)
      return
    }

    setStatus(statusList.success)

    history.push('register/success')
  }

  return (
    <LayoutOne size="small">
      <Card color="white">
        <div className="text-center mb-5">
          <StoreLogo />
        </div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl errorMessage={errors.full_name?.message}>
            <InputText
              placeholder="Nama Lengkap"
              fitContainer
              {...register('full_name', rules.full_name)}
            />
          </FormControl>
          <FormControl errorMessage={errors.email?.message}>
            <InputText
              placeholder="Email"
              fitContainer
              {...register('email', rules.email)}
            />
          </FormControl>
          <FormControl errorMessage={errors.password?.message}>
            <InputPassword
              placeholder="Password"
              fitContainer
              {...register('password', rules.password)}
            />
          </FormControl>
          <FormControl errorMessage={errors.password_confirmation?.message}>
            <InputPassword
              placeholder="Konfirmasi Password"
              fitContainer
              {...register('password_confirmation', rules.password_confirmation)}
            />
          </FormControl>
          <Button
            size="large"
            fitContainer
            disabled={status === statusList.process}
          >
            {status === statusList.process ? "Sedang memproses": "Mendaftar"}
          </Button>
        </form>
        <div className="text-center mt-2">
          Sudah punya akun? <Link to="/login"><strong>Masuk Sekarang.</strong></Link>
        </div>
      </Card>
    </LayoutOne>
  )
}
import React from 'react'
import { HashRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import 'upkit/dist/style.min.css'

import store from './app/store'
import Home from './pages/Home'

import {listen} from './app/listener'

import Register from './pages/Register'
import RegisterSuccess from "./pages/RegisterSuccess";
import Login from "./pages/Login";
import {getCart} from "./api/cart";

function App() {
  React.useEffect(() => {
    listen()
    getCart()
  }, [])
  return (
    <div>
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/register/success"><RegisterSuccess /></Route>
            <Route path="/register" component={Register} />
            <Route path="/login"><Login /></Route>
            <Route path="/" component={Home} />
          </Switch>
        </Router>
      </Provider>
    </div>
  );
}

export default App;
